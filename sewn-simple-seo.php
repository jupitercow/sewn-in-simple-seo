<?php
/*
Plugin Name: Sewn In Simple SEO
Plugin URI: http://bitbucket.org/jupitercow/sewn-in-simple-seo
Description: Adds a very simple, clean interface for controlling SEO items for a website. Requires the Advanced Custom Fields plugin.
Version: 1.0.4
Author: Jake Snyder
Author URI: http://Jupitercow.com/
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

------------------------------------------------------------------------
Copyright 2014 Jupitercow, Inc.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

if (! class_exists('sewn_seo') ) :

add_action( 'plugins_loaded', array('sewn_seo', 'plugins_loaded') );

add_action( 'init', array('sewn_seo', 'init') );

class sewn_seo
{
	/**
	 * Class prefix
	 *
	 * @since 	1.0.0
	 * @var 	string
	 */
	const PREFIX = __CLASS__;

	/**
	 * Settings
	 *
	 * @since 	1.0.0
	 * @var 	string
	 */
	public static $settings = array();

	/**
	 * Initialize the Class
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function init()
	{
		self::$settings = apply_filters( self::PREFIX . '/settings', array(
			'add_xml_sitemap' => false,
			'meta_fields' => array(
				'classification' => '<meta property="og:classification" content="%s">',
				'site_name'      => '<meta property="og:site_name" name="copyright" content="%s">',
				'title'          => '<meta property="og:title" content="%s">',
				'image'          => '<meta property="og:image" content="%s">',
				'type'           => '<meta property="og:type" content="%s">',
			),
			'messages' => array(
				'admin' => array(
					'acf' => __( 'Sewn In Simple SEO requires the <a href="http://wordpress.org/plugins/advanced-custom-fields/">Advanced Custom Fields</a> plugin to be installed and activated.', 'sewn_seo' ),
				),
			),
			'field_groups' => array(
				'title' => __( 'SEO Meta Data', 'sewn_seo' ),
				'fields' => array(
					'meta_title' => array (
						#'key' => 'field_' . self::PREFIX . '_meta_title',
						#'label' => __( 'Title', 'sewn_seo' ),
						#'name' => 'meta_title',
						#'prefix' => '',
						#'type' => 'text',
						#'instructions' => __( 'Title display in search engines is limited to 70 chars', 'sewn_seo' ),
						'key' => 'field_543562e26ddb1',
						'label' => 'Title',
						'name' => 'meta_title',
						'prefix' => '',
						'type' => 'text',
						'instructions' => 'Title display in search engines is limited to 70 chars',
						'required' => 0,
						'conditional_logic' => 0,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => 70,
						'readonly' => 0,
						'disabled' => 0,
					),
					'meta_description' => array (
						#'key' => 'field_' . self::PREFIX . '_meta_description',
						#'label' => __( 'Description', 'sewn_seo' ),
						#'name' => 'meta_description',
						#'type' => 'textarea',
						#'instructions' => __( 'The meta description is limited to 156 chars and will show up on the search engine results page.', 'sewn_seo' ),
						'key' => 'field_5435639887113',
						'label' => 'Description',
						'name' => 'meta_description',
						'prefix' => '',
						'type' => 'textarea',
						'instructions' => 'The meta description is limited to 156 chars and will show up on the search engine results page.',
						'required' => 0,
						'conditional_logic' => 0,
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => 156,
						'rows' => 4,
						'new_lines' => 'wpautop',
						'readonly' => 0,
						'disabled' => 0,
					),
					'meta_image' => array(
						'key' => 'field_' . self::PREFIX . '_meta_image',
						'label' => __( 'Open Graph Image', 'sewn_seo' ),
						'name' => 'meta_image',
						'type' => 'image',
						'instructions' => __( 'Used by Facebook when a user a shares this content.', 'sewn_seo' ),
						'save_format' => 'url',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					'xml_sitemap_exclude' => array(
						'key' => 'field_' . self::PREFIX . '_xml',
						'label' => __( 'Exclude from XML Sitemap', 'sewn_seo' ),
						'name' => 'xml_sitemap_exclude',
						'type' => 'true_false',
						'instructions' => __( 'This will keep the page from showing in the XML sitemap', 'sewn_seo' ),
						'message' => '',
						'default_value' => 0,
					),
				),
			),
		) );

		// actions
		add_action( 'wp_head',                               array(__CLASS__, 'wp_head'), 1 );
		add_action( self::PREFIX . '/meta/description',      array(__CLASS__, 'meta_description') );
		add_action( self::PREFIX . '/meta/classification',   array(__CLASS__, 'meta_classification') );
		add_action( self::PREFIX . '/meta/site_name',        array(__CLASS__, 'meta_site_name') );
		add_action( self::PREFIX . '/meta/og:title',         array(__CLASS__, 'meta_og_title') );
		add_action( self::PREFIX . '/meta/og:image',         array(__CLASS__, 'meta_og_image') );
		add_action( self::PREFIX . '/meta/og:type',          array(__CLASS__, 'meta_og_type') );
		add_action( self::PREFIX . '/meta/permalink',        array(__CLASS__, 'meta_permalink') );
		add_action( 'admin_enqueue_scripts',                 array(__CLASS__, 'admin_enqueue_scripts') );
		add_action( 'admin_init',                            array(__CLASS__, 'dependencies') );

		// filters
		add_filter( 'wp_title',                              array(__CLASS__, 'wp_title'), 99, 2 );
		add_filter( self::PREFIX . '/post_types',            array(__CLASS__, 'post_types'), 0 );
		add_filter( self::PREFIX . '/manual_image',          array(__CLASS__, 'manual_image'), 99 );

		// Load ACF Fields
		self::register_field_groups();
	}

	/**
	 * wp_head
	 *
	 * If automate is turned on, automate the header items.
	 *
	 * @author  Jake Snyder
	 * @since	1.0.3
	 * @return	void
	 */
	public static function wp_head()
	{
		if ( apply_filters( self::PREFIX . '/automate_head', true ) )
		{
			do_action('sewn_seo/meta/description');
			do_action('sewn_seo/meta/classification');
			do_action('sewn_seo/meta/site_name');
			do_action('sewn_seo/meta/og:title');
			do_action('sewn_seo/meta/og:image');
			do_action('sewn_seo/meta/og:type');
		}
	}

	/**
	 * Check for dependencies
	 *
	 * @author  Jake Snyder
	 * @since	1.0.2
	 * @return	void
	 */
	public static function dependencies()
	{
		if (! class_exists( 'acf' ) ) {
			add_action( 'admin_notices', array(__CLASS__, 'acf_dependency_message') );
		}
	}

	/**
	 * Add a nag for ACF
	 *
	 * @author  Jake Snyder
	 * @since	1.0.2
	 * @return	void
	 */
	public static function acf_dependency_message()
	{
		?>
		<div class="update-nag">
			<?php echo self::$settings['messages']['admin']['acf']; ?>
		</div>
		<?php
	}

	/**
	 * Better SEO: meta title, supported by ACF
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function post_types( $post_types )
	{
		return array( 'post','page' );
	}

	/**
	 * Image override returns false by default.
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function manual_image( $post_id )
	{
		if ( is_numeric($post_id) ) {
			return false;
		} else {
			return $post_id;
		}
	}

	/**
	 * Better SEO: meta title, supported by ACF
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function wp_title( $title, $sep="|" )
	{
		if (! $sep && false !== $sep ) $sep = "|";

		$title = "$title $sep " . get_bloginfo('blogname');
		if ( is_feed() ) return $title;

		$new_title = '';

		global $post, $paged, $page;
	
		if ( is_404() )
		{
			$new_title = apply_filters( self::PREFIX . '/404_title', "Not Found, Error 404" );
		}
		elseif ( is_home() )
		{
			$posts_page_id = get_option('page_for_posts');
			// Look for a custom meta title on a posts page
			if ( $posts_page_id && $meta = get_post_meta($posts_page_id, 'meta_title', true) ) {
				$new_title = $meta;
			// Look for a posts page post title
			} elseif ( $posts_page_id && $meta = get_the_title($posts_page_id) ) {
				$new_title = $meta . $sep . get_bloginfo('blogname');
			// Use a default that can be filtered
			} else {
				$new_title = apply_filters( self::PREFIX . '/home_title', "Blog" . $sep . get_bloginfo('blogname') );
			}
		}
		else
		{
			// Look for a custom meta title and override post title
			if (! empty($GLOBALS['post']->ID) && $meta_title = get_post_meta($GLOBALS['post']->ID, 'meta_title', true) ) {
				$new_title = $meta_title;
			}
		}

		// Add pagination
		if ( 1 < $GLOBALS['paged'] || 1 < $GLOBALS['page'] ) {
			$new_title .= " $sep Page " . max( $GLOBALS['paged'], $GLOBALS['page'] );
		}

		// Add the site name
		if ( $new_title ) {
			$title = $new_title;
		}

		return trim($title);
	}

	/**
	 * Load text limiter script in the admin for meta fields
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function admin_enqueue_scripts( $hook )
	{
		$panels = array(
			'post.php',
			'post-new.php'
		);
		if (! in_array($hook, $panels) ) return;

		wp_register_script( self::PREFIX, plugins_url( 'assets/js/scripts.js', __FILE__ ), array( 'jquery' ), '', true );
		wp_enqueue_script( self::PREFIX );
	}

	/**
	 * Better SEO: meta description, supported by ACF
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function meta_description()
	{
		$content = '';
		$field   = '<meta property="og:description" name="description" content="%s">'."\n";

		if ( is_home() && $meta = get_option('meta_description') ) {
			$content = $meta;
		} elseif (! empty($GLOBALS['post']->ID) && $meta = get_post_meta($GLOBALS['post']->ID, 'meta_description', true) ) {
			$content = $meta;
		} elseif (! empty($GLOBALS['post']->ID) && $meta = get_post_field('post_content', $GLOBALS['post']->ID) ) {
			$content = wp_trim_words($meta, '30', '');
		}

		if ( $content ) {
			printf( $field, $content );
		}
	}

	/**
	 * Better SEO: meta classification, supported by ACF
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function meta_classification()
	{
		if ( $meta = get_option('meta_classification') ) {
			printf( self::$settings['meta_fields']['classification'] . "\n", $meta );
		}
	}

	/**
	 * Better SEO: site name
	 *
	 * @author  Jake Snyder
	 * @since	1.0.1
	 * @return	void
	 */
	public static function meta_site_name()
	{
		$content = '';
		if ( $meta = get_option('meta_title') ) {
			$content = $meta;
		} else {
			$content = get_option('blogname');
		}

		if ( $content ) {
			printf( self::$settings['meta_fields']['site_name'] . "\n", $content );
		}
	}

	/**
	 * Better SEO: open graph title
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function meta_og_title()
	{
		$content = '';
		if (! empty($GLOBALS['post']->ID) && $meta = get_post_meta($GLOBALS['post']->ID, 'meta_title', true) ) {
			$content = $meta;
		} else {
			$content = get_the_title();
		}

		if ( $content ) {
			printf( self::$settings['meta_fields']['title'] . "\n", $content );
		}
	}

	/**
	 * Better SEO: open graph image
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function meta_og_image()
	{
		$content = '';
		if ( is_home() && $meta = get_option('meta_image') ) {
			$content = $meta;
		} elseif (! empty($GLOBALS['post']->ID) && $meta = apply_filters( self::PREFIX . '/manual_image', $GLOBALS['post']->ID ) ) {
			$content = $meta;
		} elseif (! empty($GLOBALS['post']->ID) && $meta = get_post_meta($GLOBALS['post']->ID, 'meta_image', true) ) {
			$content = $meta;
		} elseif (! empty($GLOBALS['post']->ID) && $meta_array = wp_get_attachment_image_src(get_post_thumbnail_id($GLOBALS['post']->ID), 'full') ) {
			if (! empty($meta_array[0]) ) {
				$content = $meta_array[0];
			}
		}

		if ( $content ) {
			printf( self::$settings['meta_fields']['image'] . "\n", $content );
		}
	}

	/**
	 * Better SEO: open graph type
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function meta_og_type()
	{
		if ( $meta = get_option('meta_type') ) {
			printf( self::$settings['meta_fields']['type'] . "\n", $meta );
		}
	}

	/**
	 * Better SEO: open graph type, supported by ACF
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function meta_permalink()
	{
		if ( is_home() )
		{
			$posts_page_id = get_option('page_for_posts');
			// Look for a permalink on a posts page
			if ( $posts_page_id && $meta = get_post_meta($posts_page_id, 'meta_title', true) ) {
				$new_title = $meta;
			// Look for a posts page post permalink
			} elseif ( $posts_page_id && $meta = get_the_title($posts_page_id) ) {
				$new_title = $meta;
			// Else home url
			} else {
				echo home_url('/');
			}
		}
		else
		{
			echo get_permalink();
		}
	}

	/**
	 * Check if other plugins are loaded and should be included
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function plugins_loaded()
	{
		if ( class_exists('sewn_xml_sitemap') ) {
			self::$settings['add_xml_sitemap'] = true;
		}
	}

	/**
	 * Better SEO: ACF SEO Fields
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function register_field_groups()
	{
		if (! function_exists("register_field_group") ) return;

		$args = array (
			'key' => self::PREFIX . '-meta-data',
			'title' => self::$settings['field_groups']['title'],
			'fields' => array (
				self::$settings['field_groups']['fields']['meta_title'],
				self::$settings['field_groups']['fields']['meta_description'],
			),
			'location' => array (),
			'menu_order' => 9999,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
		);

		if ( apply_filters( self::PREFIX . '/add_image', false ) ) {
			$args['fields'][] = self::$settings['field_groups']['fields']['meta_image'];
		}

		if ( apply_filters( self::PREFIX . '/add_xml_sitemap', self::$settings['add_xml_sitemap'] ) ) {
			$args['fields'][] = self::$settings['field_groups']['fields']['xml_sitemap_exclude'];
		}

		$default_location = array (
			'param' => 'post_type',
			'operator' => '==',
			'value' => '',
			'order_no' => 0,
			'group_no' => 0,
		);

		$post_types = apply_filters( self::PREFIX . '/post_types', array() );

		$i=0;
		foreach ( $post_types as $post_type )
		{
			$new_location = $default_location;
			$new_location['value'] = $post_type;
			$new_location['group_no'] = $i;
			$args['location'][] = array( $new_location );
			$i++;
		}

		register_field_group( $args );
	}
}

endif;