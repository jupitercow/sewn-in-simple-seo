# Sewn In Simple SEO

Creates a really simple interface for edting page meta titles for SEO. Adds Open Graph support to help control the image Facebook uses when a post is shared. Adds actions to output descriptions and keywords into your theme header. We haven't currently added keywords, we think it is best that way...

## Example of options to add to the header

```php

	<title><?php wp_title(''); ?></title>
	<?php do_action('sewn_seo/meta/description'); ?>
	<?php do_action('sewn_seo/meta/classification'); ?>
	<meta name="author" content="<?php bloginfo('name'); ?>">
	<meta name="creator" content="<?php bloginfo('name'); ?>" />
	<meta name="publisher" content="<?php bloginfo('name'); ?>" />
	<?php do_action('sewn_seo/meta/site_name'); ?>
	<?php do_action('sewn_seo/meta/og:title'); ?>
	<?php do_action('sewn_seo/meta/og:image'); ?>
	<?php do_action('sewn_seo/meta/og:type'); ?>

```

##

## Control what post types are added

By default only pages and posts are added, but you can remove either of those and/or add more using this filter:

```php
/**
 * Customize what post types are added to the XML sitemap
 *
 * @param	array	$post_types	List of post types to be added to the XML Sitemap
 * @return	array	$post_types	Modified list of post types
 */
add_filter( 'sewn_seo/post_types', 'custom_seo_post_types' );
function custom_seo_post_types( $post_types )
{
	$post_types[] = "news";
	return $post_types;
}
```

## Customize WordPress XML Sitemap

Works well with our XML Sitemap plugin, you should use them both...